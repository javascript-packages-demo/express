# express

![npm-downloads (recent)](https://badgen.net/npm/dm/express)
[express](https://www.npmjs.com/search?q=express) Fast, unopinionated, minimalist web framework for node

# Popularity
* https://survey.stackoverflow.co/2023/#most-popular-technologies-webframe
* https://insights.stackoverflow.com/survey/2020#technology-web-frameworks
